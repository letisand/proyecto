﻿namespace SALCHIPAP
{
    partial class Cuenta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxTotal_A_Pagar = new System.Windows.Forms.GroupBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.groupBoxTotal_A_Pagar.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxTotal_A_Pagar
            // 
            this.groupBoxTotal_A_Pagar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.groupBoxTotal_A_Pagar.Controls.Add(this.textBox1);
            this.groupBoxTotal_A_Pagar.Font = new System.Drawing.Font("Castellar", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxTotal_A_Pagar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.groupBoxTotal_A_Pagar.Location = new System.Drawing.Point(12, 12);
            this.groupBoxTotal_A_Pagar.Name = "groupBoxTotal_A_Pagar";
            this.groupBoxTotal_A_Pagar.Size = new System.Drawing.Size(810, 366);
            this.groupBoxTotal_A_Pagar.TabIndex = 3;
            this.groupBoxTotal_A_Pagar.TabStop = false;
            this.groupBoxTotal_A_Pagar.Text = "Cuentas pendientes";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(6, 41);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(798, 319);
            this.textBox1.TabIndex = 0;
            // 
            // Cuenta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(834, 390);
            this.Controls.Add(this.groupBoxTotal_A_Pagar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Cuenta";
            this.Text = "Cuenta";
            this.groupBoxTotal_A_Pagar.ResumeLayout(false);
            this.groupBoxTotal_A_Pagar.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBoxTotal_A_Pagar;
        private System.Windows.Forms.TextBox textBox1;
    }
}