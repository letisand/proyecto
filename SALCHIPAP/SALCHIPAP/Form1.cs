﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SALCHIPAP
{
    public partial class Ventana : Form
    {
        public Ventana()
        {
            InitializeComponent();
        }


        private void Ventana_Load(object sender, EventArgs e)
        {
            //PARA QUE EL FONDO DEL LABEL DE LA HORA SEA TRANSPARENTE
            labelHora.Parent = LOGO;
            labelHora.BackColor = Color.Transparent;
            //PARA QUE EL FONDO DEL LABEL DE LA FECHA SEA TRANSPARENTE 
            labelFecha.Parent = LOGO;
            labelFecha.BackColor = Color.Transparent;
            //PARA QUE " FECHA: " TENGA FONDO TRANSPARENTE
            //labelTextFecha.Parent = LOGO;
            // labelTextFecha.BackColor = Color.Transparent;
        }


        /////////////////MOVER LA VENTANA ////////////////////////////////////////////////////
        //VARIABLES PARA PANEL MOVE
        int posY = 0;
        int posX = 0;

        private void panelRosa_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left)//MOVIMIENTO DE LA VENTANA
            {
                posX = e.X;
                posY = e.Y;
            }
            else
            {
                Left = Left + (e.X - posX);
                Top = Top + (e.Y - posY);
            }

        }

        private void LOGO_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left)//MOVIMIENTO DE LA VENTANA
            {
                posX = e.X;
                posY = e.Y;
            }
            else
            {
                Left = Left + (e.X - posX);
                Top = Top + (e.Y - posY);
            }

        
    }
        /////////////////////////////////CLICK BOTONES////////////////////////////////////
        //BOTON CERRAR VENTANA
        private void botonX_Click(object sender, EventArgs e) //BOTON X
        {
            MessageBoxButtons botonesM = MessageBoxButtons.YesNo; //Creamos variable para los botoenes 
            DialogResult dr = MessageBox.Show("¿Seguro que deseas cerrar el programa?", " ", botonesM, MessageBoxIcon.Exclamation);
            if (dr == DialogResult.Yes)
            {
                Application.Exit();///PARA SALIR DEL PROGRAMA 
            }

        }

        //BOTON MINIMIZAR VENTANA 
        private void buttonMinimizar_Click(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Normal)
            {
                WindowState = FormWindowState.Minimized; //PARA MINIMIZAR VENTANA
            }
        }


        //BOTON CUENTA
        private void botonCuenta_Click(object sender, EventArgs e)
        {
            botonPedidos.Visible = false;
            AbrirFormHa(new Cuenta());

        }

        //BOTON SALIR 
        private void botonSalir_Click(object sender, EventArgs e)
        {
            //panelContenedor.Visible = false;
            MessageBoxButtons botonesM = MessageBoxButtons.YesNo; //Creamos variable para los botoenes 
            DialogResult dr = MessageBox.Show("¿Seguro que deseas cerrar el programa?", " ", botonesM, MessageBoxIcon.Exclamation);
            if (dr == DialogResult.Yes)
            {
                Application.Exit();///PARA SALIR DEL PROGRAMA 
            }
        }

        //BOTON PAQUETES
        public void botonPaquetes_Click(object sender, EventArgs e)
        {
            AbrirFormHa(new Paquetes());
           
        }

        //BOTON PEDIDOS
        private void botonPedidos_Click(object sender, EventArgs e)
        {
            // panelContenedor.Visible = false;
            botonPaquetes.Visible = true;
            botonCuenta.Visible = false;
            botonPedidos.Visible = false;
            AbrirFormHa(new FormularioPedidos());

        }


        private void AbrirFormHa(object form)
        {
            if (this.panelContenedor.Controls.Count > 0)
                this.panelContenedor.Controls.RemoveAt(0);
            Form fh = form as Form;
            fh.TopLevel = false;
            this.panelContenedor.Controls.Add(fh);
            this.panelContenedor.Tag = fh;
            fh.Show();
        }

        private void panelContenedor_Paint(object sender, PaintEventArgs e)
        {

        }

        private void botonRegresar_Click(object sender, EventArgs e)
        {
            if (this.panelContenedor.Controls.Count > 0)
                this.panelContenedor.Controls.RemoveAt(0);
            botonPaquetes.Visible = false;
            botonCuenta.Visible = true;
            botonPedidos.Visible = true;
            

        }
        
        private void horaFecha_Tick_1(object sender, EventArgs e)
        {

            labelHora.Text = DateTime.Now.ToShortTimeString();
            labelFecha.Text = DateTime.Now.ToShortDateString();
        }




        ///////////////////////////////FECHA

    }
}

