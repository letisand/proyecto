﻿namespace SALCHIPAP
{
    partial class Ventana
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Ventana));
            this.panelRosa = new System.Windows.Forms.Panel();
            this.panelContenedor = new System.Windows.Forms.Panel();
            this.botonRegresar = new System.Windows.Forms.Button();
            this.botonSalir = new System.Windows.Forms.Button();
            this.botonCuenta = new System.Windows.Forms.Button();
            this.botonPaquetes = new System.Windows.Forms.Button();
            this.buttonMinimizar = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.botonX = new System.Windows.Forms.Button();
            this.botonPedidos = new System.Windows.Forms.Button();
            this.LOGO = new System.Windows.Forms.PictureBox();
            this.labelHora = new System.Windows.Forms.Label();
            this.labelFecha = new System.Windows.Forms.Label();
            this.horaFecha = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.LOGO)).BeginInit();
            this.SuspendLayout();
            // 
            // panelRosa
            // 
            this.panelRosa.BackColor = System.Drawing.Color.DeepPink;
            this.panelRosa.Location = new System.Drawing.Point(0, -1);
            this.panelRosa.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panelRosa.Name = "panelRosa";
            this.panelRosa.Size = new System.Drawing.Size(878, 10);
            this.panelRosa.TabIndex = 8;
            this.panelRosa.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panelRosa_MouseMove);
            // 
            // panelContenedor
            // 
            this.panelContenedor.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panelContenedor.BackgroundImage = global::SALCHIPAP.Properties.Resources.foto1;
            this.panelContenedor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelContenedor.Location = new System.Drawing.Point(11, 200);
            this.panelContenedor.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panelContenedor.Name = "panelContenedor";
            this.panelContenedor.Size = new System.Drawing.Size(855, 411);
            this.panelContenedor.TabIndex = 9;
            this.panelContenedor.Paint += new System.Windows.Forms.PaintEventHandler(this.panelContenedor_Paint);
            // 
            // botonRegresar
            // 
            this.botonRegresar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("botonRegresar.BackgroundImage")));
            this.botonRegresar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.botonRegresar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.botonRegresar.Font = new System.Drawing.Font("Castellar", 10F, System.Drawing.FontStyle.Bold);
            this.botonRegresar.ForeColor = System.Drawing.Color.White;
            this.botonRegresar.Location = new System.Drawing.Point(628, 121);
            this.botonRegresar.Name = "botonRegresar";
            this.botonRegresar.Size = new System.Drawing.Size(80, 41);
            this.botonRegresar.TabIndex = 10;
            this.botonRegresar.UseVisualStyleBackColor = true;
            this.botonRegresar.Click += new System.EventHandler(this.botonRegresar_Click);
            // 
            // botonSalir
            // 
            this.botonSalir.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.botonSalir.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("botonSalir.BackgroundImage")));
            this.botonSalir.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.botonSalir.FlatAppearance.BorderSize = 0;
            this.botonSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.botonSalir.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.botonSalir.Location = new System.Drawing.Point(735, 121);
            this.botonSalir.Margin = new System.Windows.Forms.Padding(0);
            this.botonSalir.Name = "botonSalir";
            this.botonSalir.Size = new System.Drawing.Size(130, 41);
            this.botonSalir.TabIndex = 3;
            this.botonSalir.UseVisualStyleBackColor = true;
            this.botonSalir.Click += new System.EventHandler(this.botonSalir_Click);
            // 
            // botonCuenta
            // 
            this.botonCuenta.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.botonCuenta.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("botonCuenta.BackgroundImage")));
            this.botonCuenta.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.botonCuenta.FlatAppearance.BorderSize = 0;
            this.botonCuenta.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.botonCuenta.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.botonCuenta.Location = new System.Drawing.Point(387, 125);
            this.botonCuenta.Margin = new System.Windows.Forms.Padding(0);
            this.botonCuenta.Name = "botonCuenta";
            this.botonCuenta.Size = new System.Drawing.Size(181, 60);
            this.botonCuenta.TabIndex = 1;
            this.botonCuenta.UseVisualStyleBackColor = true;
            this.botonCuenta.Click += new System.EventHandler(this.botonCuenta_Click);
            // 
            // botonPaquetes
            // 
            this.botonPaquetes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.botonPaquetes.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("botonPaquetes.BackgroundImage")));
            this.botonPaquetes.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.botonPaquetes.FlatAppearance.BorderSize = 0;
            this.botonPaquetes.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.botonPaquetes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.botonPaquetes.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.botonPaquetes.Location = new System.Drawing.Point(16, 121);
            this.botonPaquetes.Margin = new System.Windows.Forms.Padding(0);
            this.botonPaquetes.Name = "botonPaquetes";
            this.botonPaquetes.Size = new System.Drawing.Size(181, 64);
            this.botonPaquetes.TabIndex = 4;
            this.botonPaquetes.UseVisualStyleBackColor = true;
            this.botonPaquetes.Visible = false;
            this.botonPaquetes.Click += new System.EventHandler(this.botonPaquetes_Click);
            // 
            // buttonMinimizar
            // 
            this.buttonMinimizar.BackColor = System.Drawing.Color.Transparent;
            this.buttonMinimizar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonMinimizar.BackgroundImage")));
            this.buttonMinimizar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonMinimizar.FlatAppearance.BorderSize = 0;
            this.buttonMinimizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonMinimizar.ForeColor = System.Drawing.Color.PaleGreen;
            this.buttonMinimizar.Location = new System.Drawing.Point(782, 24);
            this.buttonMinimizar.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonMinimizar.Name = "buttonMinimizar";
            this.buttonMinimizar.Size = new System.Drawing.Size(22, 19);
            this.buttonMinimizar.TabIndex = 7;
            this.buttonMinimizar.UseVisualStyleBackColor = false;
            this.buttonMinimizar.Click += new System.EventHandler(this.buttonMinimizar_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.button1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button1.BackgroundImage")));
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.ForeColor = System.Drawing.Color.PaleGreen;
            this.button1.Location = new System.Drawing.Point(422, 303);
            this.button1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(0, 0);
            this.button1.TabIndex = 6;
            this.button1.UseVisualStyleBackColor = false;
            // 
            // botonX
            // 
            this.botonX.BackColor = System.Drawing.Color.Transparent;
            this.botonX.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("botonX.BackgroundImage")));
            this.botonX.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.botonX.FlatAppearance.BorderSize = 0;
            this.botonX.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.botonX.ForeColor = System.Drawing.Color.PaleGreen;
            this.botonX.Location = new System.Drawing.Point(818, 24);
            this.botonX.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.botonX.Name = "botonX";
            this.botonX.Size = new System.Drawing.Size(22, 19);
            this.botonX.TabIndex = 5;
            this.botonX.UseVisualStyleBackColor = false;
            this.botonX.Click += new System.EventHandler(this.botonX_Click);
            // 
            // botonPedidos
            // 
            this.botonPedidos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.botonPedidos.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("botonPedidos.BackgroundImage")));
            this.botonPedidos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.botonPedidos.FlatAppearance.BorderSize = 0;
            this.botonPedidos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.botonPedidos.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.botonPedidos.Location = new System.Drawing.Point(202, 123);
            this.botonPedidos.Margin = new System.Windows.Forms.Padding(0);
            this.botonPedidos.Name = "botonPedidos";
            this.botonPedidos.Size = new System.Drawing.Size(181, 63);
            this.botonPedidos.TabIndex = 2;
            this.botonPedidos.UseVisualStyleBackColor = true;
            this.botonPedidos.Click += new System.EventHandler(this.botonPedidos_Click);
            // 
            // LOGO
            // 
            this.LOGO.Image = global::SALCHIPAP.Properties.Resources.logo_imagen;
            this.LOGO.Location = new System.Drawing.Point(6, 14);
            this.LOGO.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.LOGO.Name = "LOGO";
            this.LOGO.Size = new System.Drawing.Size(864, 100);
            this.LOGO.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.LOGO.TabIndex = 0;
            this.LOGO.TabStop = false;
            this.LOGO.MouseMove += new System.Windows.Forms.MouseEventHandler(this.LOGO_MouseMove);
            // 
            // labelHora
            // 
            this.labelHora.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.labelHora.AutoSize = true;
            this.labelHora.BackColor = System.Drawing.Color.Transparent;
            this.labelHora.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labelHora.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelHora.Location = new System.Drawing.Point(698, 50);
            this.labelHora.Margin = new System.Windows.Forms.Padding(0);
            this.labelHora.Name = "labelHora";
            this.labelHora.Size = new System.Drawing.Size(74, 29);
            this.labelHora.TabIndex = 11;
            this.labelHora.Text = "Hora:";
            // 
            // labelFecha
            // 
            this.labelFecha.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.labelFecha.AutoSize = true;
            this.labelFecha.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFecha.Location = new System.Drawing.Point(700, 79);
            this.labelFecha.Name = "labelFecha";
            this.labelFecha.Size = new System.Drawing.Size(62, 19);
            this.labelFecha.TabIndex = 12;
            this.labelFecha.Text = "Fecha:";
            // 
            // horaFecha
            // 
            this.horaFecha.Enabled = true;
            this.horaFecha.Tick += new System.EventHandler(this.horaFecha_Tick_1);
            // 
            // Ventana
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(877, 612);
            this.Controls.Add(this.labelFecha);
            this.Controls.Add(this.labelHora);
            this.Controls.Add(this.botonRegresar);
            this.Controls.Add(this.botonSalir);
            this.Controls.Add(this.botonCuenta);
            this.Controls.Add(this.botonPaquetes);
            this.Controls.Add(this.panelContenedor);
            this.Controls.Add(this.panelRosa);
            this.Controls.Add(this.buttonMinimizar);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.botonX);
            this.Controls.Add(this.botonPedidos);
            this.Controls.Add(this.LOGO);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.MaximizeBox = false;
            this.Name = "Ventana";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "3";
            this.Load += new System.EventHandler(this.Ventana_Load);
            ((System.ComponentModel.ISupportInitialize)(this.LOGO)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox LOGO;
        private System.Windows.Forms.Button botonCuenta;
        private System.Windows.Forms.Button botonPedidos;
        private System.Windows.Forms.Button botonSalir;
        private System.Windows.Forms.Button botonX;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button buttonMinimizar;
        private System.Windows.Forms.Panel panelRosa;
        private System.Windows.Forms.Panel panelContenedor;
        private System.Windows.Forms.Button botonPaquetes;
        private System.Windows.Forms.Button botonRegresar;
        private System.Windows.Forms.Label labelHora;
        private System.Windows.Forms.Label labelFecha;
        private System.Windows.Forms.Timer horaFecha;
    }
}

